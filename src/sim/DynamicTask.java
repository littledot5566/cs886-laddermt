package sim;

import java.util.Vector;

/*
 * Author: Tzu-Yang (Ben). Yu
 *
 */

public class DynamicTask {
	// paid base on the agents rank
	private Vector<Integer> pay = new Vector<Integer>();
	private Vector<Agent> attendanceList = new Vector<Agent>();
	private boolean isNewTask = true;
	private int totalTask = 0;
	private int rankLevelsNum = 5;	// default: 5 levels of rank. e.g. 0.1.2.3...
	private int tasksCompletedForLevelUp = 5;

	// "corretnessForLevelUp" means an agent need to have at least 90% correctness
	// on given task in oder to move to the next rank
	private double corretnessForLevelUp = 0.9;

	public DynamicTask(Vector<Integer> pay, int task, int rankLevelsNum) {
		this.pay = pay;
		this.totalTask = task;
		this.rankLevelsNum = rankLevelsNum;
	}

	/**
	 * 
	 * @param agentList
	 * @return Author: Ben
	 */
	public int performed(Vector<Agent> agentList) {
		Vector<Agent> newAttendanceList = new Vector<Agent>();
		if (isNewTask) {
			agentList = updateAgentStatus(agentList, isNewTask);
			attendanceList = agentList;
			totalTask -= agentList.size();
			isNewTask = false;
		} else {
			for (Agent a : agentList) {
				if (a.getTasksCompleted() == tasksCompletedForLevelUp) {
					double agentCorrectnessThreshould = a.getCorrectnessRate();
					// corretnessForLevelUp = Math.random();
					if (a.getRankStatus() < rankLevelsNum - 1
							&& agentCorrectnessThreshould >= corretnessForLevelUp) {
						a.setRankStatus(a.getRankStatus() + 1);
					} else if (a.getRankStatus() == rankLevelsNum - 1
							&& agentCorrectnessThreshould >= corretnessForLevelUp) {
						// System.out.println("An agent reach the max rank");
					}
					a.setTasksCompleted(0);
				}
				agentList = updateAgentStatus(agentList, isNewTask);
				double agentReturnThreshold = a.getUtilityFactor() * a.getPaid();
				if (agentReturnThreshold >= (int) (100 * Math.random())) {
					a.setTasksCompleted(a.getTasksCompleted() + 1);
					if (totalTask > 0) {
						totalTask -= 1;
					} else { // no tasks for agents
						break;
					}
					newAttendanceList.add(a);
				}
			}
			attendanceList.addAll(newAttendanceList);
		}

		return totalTask;
	}

	/**
	 * 
	 * @param agentList
	 * @param isNewTask
	 * @return Author: Ben
	 */
	private Vector<Agent> updateAgentStatus(Vector<Agent> agentList,
			boolean isNewTask) {

		if (pay.size() != 0) {
			if (isNewTask) {
				for (Agent a : agentList) {
					a.setPaid(pay.get(0));
					a.setTasksCompleted(1);
					a.setRankStatus(0);
				}
			} else {
				for (Agent a : agentList) {
					// System.out.println(a.getRankStatus());
					a.setPaid(pay.get(a.getRankStatus()));
				}
			}
		}
		return agentList;
	}

	/**
	 * 
	 * @return average Correctness Authors: Ben
	 */
	public double getAverageCorrectness() {
		double averageCorrectness = 0;
		for (Agent a : attendanceList) {
			averageCorrectness += a.getCorrectnessRate();
		}
		averageCorrectness = averageCorrectness / attendanceList.size();
		return averageCorrectness;
	}

	// getters and setters
	public int getTotalTask() {
		return totalTask;
	}
}
