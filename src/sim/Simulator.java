package sim;

import java.util.Vector;

/*
 * Author: Tzu-Yang (Ben). Yu
 *
 */

public class Simulator {
	public int taskNum;
	public int numOfAgents;
	public int staticTaskPay;
	public int rankLevelsNum;
	public int tasksCompletedForLevelUp = 0;

	public Vector<Agent> agentList = new Vector<Agent>();
	public Vector<Integer> dynamicTaskPay = new Vector<Integer>();

	/**
	 * 
	 * @param taskNum
	 * @param numOfAgents
	 * @param staticTaskPay
	 * @param dynamicTaskPay
	 * @param rankLevelsNum
	 */
	public Simulator(int taskNum, int numOfAgents, int staticTaskPay) {
		this.taskNum = taskNum;
		this.numOfAgents = numOfAgents;
		this.staticTaskPay = staticTaskPay;
		System.out.format("taskNum=%d numAgents=%d staticTaskPay=%d\n", taskNum,
				numOfAgents, staticTaskPay);

	}

	public void runStaticSimulation() {
		StaticTask st = new StaticTask(staticTaskPay, taskNum);
		this.agentList = genAgents(numOfAgents);
		int remainTask = st.getTotalTask();
		while (remainTask > 0) {
			remainTask = st.performed(agentList);
		}
		System.out.println("Average Correctness for static task : "
				+ st.getAverageCorrectness());
	}

	/**
	 * 
	 * @param dynamicPaid
	 * @param levels
	 * @param tasksCompletedForLevelUp
	 *          Author: Ben
	 */
	public void runDynamicSimulation(Vector<Integer> dynamicPaid, int levels,
			int tasksCompletedForLevelUp) {

		this.dynamicTaskPay = dynamicPaid;
		this.rankLevelsNum = levels;
		this.tasksCompletedForLevelUp = tasksCompletedForLevelUp;

		DynamicTask dt = new DynamicTask(dynamicTaskPay, taskNum, rankLevelsNum);
		this.agentList = genAgents(numOfAgents);
		int remainTask = dt.getTotalTask();
		while (remainTask > 0) {
			remainTask = dt.performed(agentList);
		}
		System.out.println("Average Correctness for dynamic task : "
				+ dt.getAverageCorrectness());
	}

	/**
	 * @param numOfAgents
	 * @return listOfAgents Author: Ben
	 */
	private Vector<Agent> genAgents(int numOfAgents) {
		Vector<Agent> listOfAgents = new Vector<Agent>();
		for (int i = 1; i <= numOfAgents; i++) {
			String agentName = "a" + i;
			listOfAgents.add(new Agent(agentName));
		}
		return listOfAgents;
	}
}
