package sim;

import java.util.Vector;

/*
 * Author: Tzu-Yang (Ben). Yu
 *
 */

public class StaticTask {
	private int pay = 50;									// default = 50;
	private Vector<Agent> attendanceList = new Vector<Agent>();
	private boolean isNewTask = true;
	private int totalTask = 0;

	/**
	 * 
	 * @param pay
	 * @param task
	 */
	public StaticTask(int pay, int task) {
		this.pay = pay;
		this.totalTask = task;
	}

	/**
	 * 
	 * @param agentList
	 *          What happen after agent perform this task Authors: Ben
	 * @param isNewTask
	 * @return the remain tasks
	 */
	public int performed(Vector<Agent> agentList) {
		Vector<Agent> newAttendanceList = new Vector<Agent>();
		agentList = setPaidForAgents(agentList);

		if (isNewTask) {
			attendanceList = agentList;
			totalTask -= agentList.size();
			isNewTask = false;
		} else {
			for (Agent a : agentList) {
				double agentReturnThreshold = a.getUtilityFactor() * a.getPaid();
				if (agentReturnThreshold >= (100 * Math.random())) {
					newAttendanceList.add(a);
					if (totalTask > 0)
						totalTask -= 1;
				}
			}
			attendanceList.addAll(newAttendanceList);
		}

		return totalTask;
	}

	/**
	 * 
	 * @return average Correctness Authors: Ben
	 */
	public double getAverageCorrectness() {
		double averageCorrectness = 0;
		for (Agent a : attendanceList) {
			averageCorrectness += a.getCorrectnessRate();
		}
		averageCorrectness = averageCorrectness / attendanceList.size();
		return averageCorrectness;
	}

	private Vector<Agent> setPaidForAgents(Vector<Agent> agentList) {
		for (Agent a : agentList) {
			a.setPaid(pay);
		}

		return agentList;
	}

	// getter and setter
	public int getPay() {
		return pay;
	}

	public void setPay(int pay) {
		this.pay = pay;
	}

	public int getTotalTask() {
		return totalTask;
	}
}
