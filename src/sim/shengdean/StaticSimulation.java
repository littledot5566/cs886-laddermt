package sim.shengdean;

import java.util.ArrayList;
import java.util.Random;

public class StaticSimulation {

	public int _numTasks;
	public int _numAgents;
	public double _basePay;

	ArrayList<Agent> _agents = new ArrayList<Agent>();
	ArrayList<Task> _tasks = new ArrayList<Task>();

	public StaticSimulation(int tasks, ArrayList<Agent> agents, double pay) {
		_numTasks = tasks;
		_numAgents = agents.size();
		_basePay = pay;

		// generate agents
		for (Agent agent : agents) {
			Agent copy = new Agent(agent._id, agent._mean, agent._variance);
			_agents.add(copy);
		}

		// generate tasks
		for (int i = 0; i < tasks; i++) {
			_tasks.add(new Task(i, pay));
		}
	}

	public void run() {
		Random rand = new Random();
		double sum = 0;

		for (int i = 0; i < _numTasks; i++) {
			// random worker is assigned
			int agentID = rand.nextInt(_numAgents);
			Agent agent = _agents.get(agentID);
			Task task = _tasks.get(i);

			double res = agent.doTask(task);

			sum += res;
		}

		double avg = sum / _numTasks;
		System.out.format("res: %f / %d = %f\npay: %f\n", sum, _numTasks, avg,
				_basePay * _numTasks);
	}

	/**
	 * Generate charts.
	 */
	public void chart() {
		Charter.chartQuality(_agents);
		Charter.chartPay(_agents);
		Charter.chartScore(_agents);
	}
}
