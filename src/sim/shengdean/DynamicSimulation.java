package sim.shengdean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class DynamicSimulation {

	public int _numTasks;
	public int _numAgents;
	public double _minPay;
	public double _maxPay;
	public double _avgPay;

	ArrayList<Agent> _agents = new ArrayList<Agent>();

	ArrayList<Task> _tasks = new ArrayList<Task>();
	public int _taskIndex; // next incomplete task

	public int _payStrategy;
	ArrayList<Integer> _brackets = new ArrayList<Integer>();

	public DynamicSimulation(int tasks, ArrayList<Agent> agents,
			double minPay, double maxPay, double avgPay) {

		if (minPay > maxPay)
			throw new IllegalArgumentException("minPay > maxPay");

		_numTasks = tasks;
		_numAgents = agents.size();
		_minPay = minPay;
		_maxPay = maxPay;
		_avgPay = avgPay;

		// generate agents
		for (Agent agent : agents) {
			Agent copy = new Agent(agent._id, agent._mean, agent._variance);
			_agents.add(copy);
		}

		// generate tasks
		for (int i = 0; i < tasks; i++) {
			_tasks.add(new Task(i));
		}
	}

	public void run() {
		run1();

		int count = 1;
		boolean more;
		do {
			more = run2();
			count ++;
		} while (more);
//		System.out.println("round: " + count);
	}

	private void run1() {
		// all agents do 1 task
		for (int i = 0; i < _numAgents; i++) {
			Agent agent = _agents.get(i);
			Task task = _tasks.get(i);

			agent.doTask(task);
			agent.calculateScore();
		}

		// sort agents based on ranking
		Collections.sort(_agents, Collections.reverseOrder());

		_taskIndex = _numAgents;
	}

	private boolean run2() {
		Random rand = new Random();

		// subsequent rounds
		for (int i = 0; i < _numAgents; i++) {

			Agent agent = _agents.get(i);
			// XXX: calculate adjusted pay
			Double factor = agent._pastScore.get(agent._pastScore.size()-1)/5;
//			double factor = (_numAgents - i) / (double) (_numAgents);
			double pay = _minPay + (double) (_maxPay - _minPay) * factor;

			
			// calculate agent's attraction to the task
			double attraction = agent.calculateAttraction(pay, _avgPay);
			double disgust = rand.nextDouble();
			// System.out.format("attr=%f disg=%f\n", attraction, disgust);

			// agent accepts the task
			if (disgust < attraction) {
				Task task = _tasks.get(_taskIndex++);
				task._pay = pay;

				agent.doTask(task);
				agent.calculateScore();

				// no more tasks
				if (_taskIndex == _tasks.size()) {
					Collections.sort(_agents, Collections.reverseOrder());
					return false;
				}
			}
		}

		Collections.sort(_agents, Collections.reverseOrder());
		return true;
	}

	public void report() {
		double totalRes = 0;
		double totalPay = 0;

		for (int i = 0; i < _numTasks; i++) {
			Task task = _tasks.get(i);
			totalRes += task._quality;
			totalPay += task._pay;
		}

		double avgRes = totalRes / _numTasks;
		System.out.format("res: %f / %d = %f\npay: %f\n", totalRes, _numTasks,
				avgRes, totalPay);
	}

	/**
	 * Generate charts.
	 */
	public void chart() {
		Charter.chartQuality(_agents);
		Charter.chartPay(_agents);
		Charter.chartScore(_agents);
	}
}
