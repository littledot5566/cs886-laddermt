package sim.shengdean;

import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

public class Charter {

	public static void chartQuality(ArrayList<Agent> agents) {

		DefaultCategoryDataset qaData = new DefaultCategoryDataset();

		for (int i = 0; i < agents.size(); i++) {
			Agent agent = agents.get(i);
			double totalQA = 0;

			// calculate avg quality for each agent
			for (int j = 0; j < agent._completed.size(); j++) {
				Task task = agent._completed.get(j);
				totalQA += task._quality;
			}

			// compare avg quality and mean quality
			qaData.addValue(totalQA / agent._completed.size(), "Avg Quality", "" + i);
			qaData.addValue(agent._mean, "Expected Avg Quality", "" + i);
		}

		JFreeChart chart = ChartFactory.createBarChart("Quality Chart", // chart
																																		// title
				"Agent ID", // domain axis label
				"Quality", // range axis label
				qaData, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips?
				false // URLs?
				);

		CategoryPlot plot = chart.getCategoryPlot();
		final CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);

		ChartFrame frame = new ChartFrame("", chart);
		frame.pack();
		frame.setVisible(true);
	}

	public static void chartPay(ArrayList<Agent> agents) {
		DefaultCategoryDataset payData = new DefaultCategoryDataset();
		DefaultCategoryDataset taskData = new DefaultCategoryDataset();

		for (int i = 0; i < agents.size(); i++) {
			Agent agent = agents.get(i);
			double totalPay = 0;

			for (int j = 0; j < agent._completed.size(); j++) {
				Task task = agent._completed.get(j);
				totalPay += task._pay;
			}

			taskData.addValue(agent._completed.size(), "No of Tasks", "" + i);
			payData.addValue(totalPay, "Total Pay", "" + i);
		}

		JFreeChart payChart = ChartFactory.createBarChart("Work Chart", // chart
																																		// title
				"Agent ID", // domain axis label
				"$", // range axis label
				payData, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips?
				false // URLs?
				);

		CategoryPlot plot = payChart.getCategoryPlot();
		plot.setDataset(1, taskData);
		plot.mapDatasetToRangeAxis(1, 1);

		final CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
		final ValueAxis axis2 = new NumberAxis("No of Tasks Completed");
		plot.setRangeAxis(1, axis2);

		final LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
		renderer2.setToolTipGenerator(new StandardCategoryToolTipGenerator());
		plot.setRenderer(1, renderer2);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);

		ChartFrame payFrame = new ChartFrame("", payChart);
		payFrame.pack();
		payFrame.setVisible(true);
	}

	public static void chartScore(ArrayList<Agent> agents) {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//TODO
		for (int i = 0; i < agents.size(); i++) {
//			if (i == 0 || i == (agents.size() / 2) || i == (agents.size() - 1)) {
				ArrayList<Double> scores = agents.get(i)._pastScore;

				for (int j = 0; j < scores.size(); j++) {
					dataset.addValue(scores.get(j), "" + i, "" + j);
				}
//			}
		}

		JFreeChart chart = ChartFactory.createLineChart("Score Chart", // chart
																																		// title
				"no. of completing tasks", // domain axis label
				"Score", // range axis label
				dataset, // data
				PlotOrientation.VERTICAL, // orientation
				true, // include legend
				true, // tooltips
				false // urls
				);

		ChartFrame frame = new ChartFrame("", chart);
		frame.pack();
		frame.setVisible(true);
	}
}
