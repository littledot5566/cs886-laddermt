package sim;

import java.util.ArrayList;
import java.util.Vector;

import sim.shengdean.Agent;
import sim.shengdean.DynamicSimulation;
import sim.shengdean.StaticSimulation;

/*
 * Author: Tzu-Yang (Ben). Yu
 *
 */

public class Main {

	public static int tasks = 100;
	public static int agents = 5;
	public static double avgPay = 50;
	public static double minPay = 20;
	public static double maxPay = 60;
	public static double agntVariance = 0.2;

	public static void main(String[] args) {
		Vector<Integer> dynamicPaid = new Vector<Integer>();
		int levels = 5;
		int tasksCompletedForLevelUp = 5;
		for (int i = 0; i < levels; i++) {
			int factor = i * 10;
			dynamicPaid.add(40 + factor);
		}
		

		// Simulator s = new Simulator(10000, 50, 50); // 10000 tasks, 50 agents, $50 flat pay
		// s.runStaticSimulation();
		// s.runDynamicSimulation(dynamicPaid, levels, tasksCompletedForLevelUp);

		// generate agents
		ArrayList<Agent> _agents = new ArrayList<Agent>();
//		for (int i = 0; i < agents; i++) {
//			_agents.add(new Agent(i, Math.random(), agntVariance));
//		}
		
		double factor = (double) 1 / agents;
		double minIntel = factor / 2;
		for (int i = 0; i < agents; i++) {
			Agent a =  new Agent(i, minIntel + factor * i, agntVariance);
			_agents.add(a);
		}

		// ShengDean static sim
		for (int i = 0; i < 10; i++) {

			StaticSimulation ss = new StaticSimulation(
					tasks,
					_agents,
					avgPay);

			// ShengDean dynamic sim

			DynamicSimulation ds = new DynamicSimulation(
					tasks,
					_agents,
					minPay, maxPay, avgPay);
//			System.err.println("STATIC" + i);
			System.out.println("STATIC" + i);
			ss.run();
//			ss.chart();
//			System.err.println("DYNAMIC" + i);
			System.out.println("DYNAMIC" + i);
			ds.run();
			ds.report();
//			ds.chart();
		}
	}
}
