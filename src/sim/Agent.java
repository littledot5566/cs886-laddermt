package sim;

import cern.jet.random.Normal;
import cern.jet.random.engine.RandomEngine;

/*
 * Author: Tzu-Yang (Ben). Yu
 *
 */

public class Agent {
	// Sheng-Dean: agent's answers should form a normal distribution
	public int _id;
	public double _mean;
	public double _variance;
	public Normal _dist;

	// ben
	public double correctnessRate;
	public final int utilityFactor = 1;
	public String name;
	public double paid = 0;
	public int rankStatus = 0;	// 0 is the lowest
	public int tasksCompleted = 0;

	public Agent(int id, double mean, double var) {
		_id = id;
		_mean = mean;
		_variance = var;
		_dist = new Normal(_mean, _variance, RandomEngine.makeDefault());
	}

	public Agent(String name) {
		this.name = name;
		correctnessRate = Math.random();
	}

	// getter and setter
	public double getCorrectnessRate() {
		return correctnessRate;
	}

	public int getUtilityFactor() {
		return utilityFactor;
	}

	public String getName() {
		return name;
	}

	public double getPaid() {
		return paid;
	}

	public void setPaid(double paid) {
		this.paid = paid;
	}

	public int getRankStatus() {
		return rankStatus;
	}

	public void setRankStatus(int rankStatus) {
		this.rankStatus = rankStatus;
	}

	public int getTasksCompleted() {
		return tasksCompleted;
	}

	public void setTasksCompleted(int tasksCompleted) {
		this.tasksCompleted = tasksCompleted;
	}

}
